package com.fpickzoom;

import java.util.ArrayList;
import java.util.Collection;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

public class ProfilePictureActivity extends Activity{
	
	ProfilePictureView profilePic;
	private TextView selectedUserTextView;
	private GraphUser selectedUser;
	private String userName;
	private String userId;
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_pic_activity);
		View currentView = this.findViewById(android.R.id.content);
		profilePic = (ProfilePictureView) currentView.findViewById(R.id.profilePicture);
		selectedUserTextView = (TextView) currentView.findViewById(R.id.friendNameTextView);
		
		profilePic.setCropped(true);
		profilePic.setPresetSize(ProfilePictureView.LARGE);
		
		//Get user information from FriendPickerApplication
        FriendPickerApplication application = (FriendPickerApplication) getApplication();
        Collection<GraphUser> selection = application.getSelectedUsers();
        if (selection != null && selection.size() > 0) {
            for (GraphUser user : selection) {
            	selectedUser = user;
            	userId = selectedUser.getId();
                break;
            }
            userName = selectedUser.getName();
        } else {
        	//In case activity called with no user (unlikely)
            selectedUser = null;
            userName = "<No selected user>";
            userId = "zuck";
        }
		profilePic.setProfileId(userId);

	}
	
    @Override
    protected void onStart() {
        super.onStart();

        // Update display to current user's name
		selectedUserTextView.setText(userName);
		//Set the profile picture to the selected user
    }

}
