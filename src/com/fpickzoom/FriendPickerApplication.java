package com.fpickzoom;

import java.util.Collection;

import android.app.Application;

import com.facebook.model.GraphUser;

// We use a custom Application class to store our minimal state data (which users have been selected).
// A real-world application will likely require a more robust data model.
public class FriendPickerApplication extends Application {
    private Collection<GraphUser> selectedUser;

    public Collection<GraphUser> getSelectedUsers() {
        return selectedUser;
    }

    public void setSelectedUsers(Collection<GraphUser> selectedUser) {
        this.selectedUser = selectedUser;
    }
}
